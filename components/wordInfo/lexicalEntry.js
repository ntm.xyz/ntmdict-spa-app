import React from 'react';
import {
  Card
} from 'react-onsenui';
import { Pronunciation } from './pronunciation';
import Styles from './wordInfo.scss';
import { Sense } from './sense';

export class LexicalEntry extends React.PureComponent {
  render() {
    const { lexical } = this.props;
    return <Card>
      <h3 className={Styles.lexicalTitle}>
        {lexical.lexicalCategory}
        <span className={Styles.lexicalPronunciation}>{lexical.pronunciations && lexical.pronunciations.map((pron, index) =>
          <Pronunciation pron={pron} key={index} />
        )}</span>
      </h3>
      <p>{lexical.etymologies}</p>
      {lexical.entries && lexical.entries.map((entry, index) => <ol className={Styles.lexicalEntries} key={index}>{entry.senses.map((sense, index) =>
        <Sense sense={sense} key={index}/>
      )}</ol>)}

    </Card>;
  }
}
