import React from 'react';
import ReactDom from 'react-dom';
import {
  Icon
} from 'react-onsenui';

import Styles from './wordInfo.scss';

export class Pronunciation extends React.PureComponent {
  onAudioRef = (ref) => {
    this.audioRef = ReactDom.findDOMNode(ref);
  };

  onSpeakerClick = () => {
    if (this.audioRef) {
      this.audioRef.play();
    }
  };

  render() {
    const { pron } = this.props;
    return <span className={Styles.pronunciation}>
      <span className={Styles.pronunciationIpa}>{pron.phoneticSpelling}</span>
      <a className={Styles.pronunciationSpeaker} onClick={this.onSpeakerClick}>
        <audio ref={this.onAudioRef} src={pron.audioFile}></audio>
        <Icon
          icon={{default: 'volume-up'}}
        />
      </a>
    </span>;
  }
}
