import React from 'react';
import { LexicalEntry } from './lexicalEntry';

import Styles from './wordInfo.scss';

export class WordInfo extends React.Component {
  render() {
    const { word } = this.props;
    console.log('word.lexicalEntries', word.lexicalEntries);
    return <div className={Styles.wordInfoWrapper}>
      <h2 className={Styles.wordInfoTitle}>{word.word}</h2>
      {word.lexicalEntries && word.lexicalEntries.map((lexical, index) => <LexicalEntry key={index} lexical={lexical} />)}
    </div>;
  }
}
