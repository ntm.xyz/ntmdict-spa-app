import {combineReducers} from 'redux';
import { MainPageSearchReducer } from '../senses/mainPage/mainPage.reducer';

const combinedReducer = combineReducers({
  MainPageSearchReducer
});

export default combinedReducer;
