import { createAction } from '../../util/reduxHelper';

export const MainPageSearchAction = createAction('MAIN_PAGE_SEARCH');

export const fetchWord = (word) => {
  return MainPageSearchAction.createStart(word);
};
