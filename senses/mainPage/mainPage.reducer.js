import { createReducer } from '../../util/reduxHelper';
import { MainPageSearchAction } from './mainPage.action';

export const MainPageSearchReducer = createReducer('MAIN_PAGE_SEARCH', MainPageSearchAction, {
  defaultState: {
    isLoading: false,
    word: null,
    error: null
  },
  startCb: (state, action) => {
    return {
      ...state,
      isLoading: true,
      error: null
    };
  },
  successCb: (state, action) => {
    return {
      ...state,
      isLoading: false,
      word: action.payload,
      error: null
    };
  },
  failureCb: (state, action) => ({
    ...state,
    isLoading: false,
    word: null,
    error: action.payload
  }),
  cancelCb: (state, action) => ({
    ...state,
    isLoading: false,
    word: null,
    error: null
  })
});
