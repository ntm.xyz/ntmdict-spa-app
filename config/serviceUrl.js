const ENV = 'pro';
const urls = {
  pro: 'https://ntmdict.herokuapp.com/dict'
};

export const getTranslationUrl = (lang, word) => {
  return `${urls[ENV]}/${lang}/${word}`;
};
